# Voraussetzungen
Docker installiert

Alternativ: https://www.katacoda.com/courses/docker/deploying-first-container https://bit.ly/2TCVbxb


Cheatsheet: https://bit.ly/2RTfeaw

# Aufgabe 1 
a) Lade das Image nginx herunter

b) Starte einen Container  mit dem nginx Image 
* Der Port 80 muss nach außen durchgelassen werden
* Überprüfe via `curl docker` ob die nginx Startseite verfügbar ist

c) Verbinde dich in den Container und füge der index.html einen beliebigen Inhalt hinzu `/usr/share/nginx/html`

# Aufgabe 2
* Pulle das Image lweiss89/schulung-app
* Starte ein Container mit dem soeben geladenen Image
* Teste die vorhandenen Schnittstellen mittels curl `(curl docker:8080/ROUTE)`